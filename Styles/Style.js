import {StyleSheet} from "react-native";

export const styles = StyleSheet.create({
    container: {
        flex: 1,
        flexDirection: "column",
        backgroundColor: "#3077ff"
    },
    resultText: {
        fontSize: 50,
        color: "black"
    },
    result: {
        justifyContent: "flex-end",
        alignItems: "flex-end",
        marginRight: 5
    },
    keys: {
        backgroundColor: "#303030",
        flexDirection: 'row',
        flexWrap: 'wrap',
        alignContent:'stretch'
    }
});

export const stylesPortrait = StyleSheet.create({
    result: {
        flex: 0.5,
    },
    keys: {
        flex: 0.5,
    },
});

export const stylesLandscape = StyleSheet.create({
    result: {
        flex: 0.3,
    },
    keys: {
        flex: 0.7,
    },
});
