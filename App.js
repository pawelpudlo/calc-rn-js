import { StatusBar } from 'expo-status-bar';
import React from 'react';
import { View,Text,Dimensions,SafeAreaView } from 'react-native';
import Button from "./components/Button";
import {styles,stylesPortrait,stylesLandscape} from "./Styles/Style";
import {createSquare} from "react-native/Libraries/StyleSheet/Rect";


const Orient = {
    portrait: 'portrait',
    landscape: 'landscape'
};

export default class App extends React.Component{

    blockedValue = 0;

    sumbit = '';

     variable  = {
        display: 0,
        prev: '',
        operation: ''
    };

    onChange = ({window: {width, height}}) => {
        let isPortrait = false;

        if (width < height) {
            isPortrait = true;
        }

        this.setState({
            ...this.variable,
            isPortrait
        })
    };

    componentDidMount() {
        const dimensions = {
            window: {
                width: Dimensions.get('window').width,
                height: Dimensions.get('window').height
            }
        };

        this.onChange(dimensions);

        Dimensions.addEventListener("change", this.onChange);
    }

    componentWillUnmount() {
        Dimensions.removeEventListener("change", this.onChange);
    }

    handleNumber = (number) => {

        if (this.variable.display === 0 && number !== ',') {
            this.setState({
                ...this.variable,
                display: number
            });
            this.variable.display = number;

        } else {
            if (number === ',' && !this.variable.display.toString().includes('.')) {
                this.setState({
                    ...this.variable,
                    display: number,

                });
                this.variable.display =  this.variable.display + ".";

            } else{
                this.setState({
                    ...this.variable,
                    display: parseFloat(this.variable.display.toString() + number),

                });
                if(number !== ','){
                    this.variable.display = this.variable.display + "" + number;
                }
            }
        }
        this.setState({
            ...this.variable,
            operation: ''

        });
        this.variable.operation = '';
    };

    handleOperation = (operation) => {
        let {display, prev} = this.state;

        if (prev !== '' && this.variable.operation === '') {
            this.handleEquals();
            setTimeout(() => {
                display = this.variable.display;
                this.setState({
                    ...this.variable,
                    operation: operation,
                    prev: prev + display,
                    display: 0
                })
            }, 1);
            this.variable.prev = this.variable.prev + this.variable.display +operation;
            this.variable.display = 0;
            this.variable.operation = operation
        } else if (this.variable.prev === '' && this.variable.operation ===''){
            this.setState({
                ...this.state,
                operation: operation,
                prev: display,
                display: 0
            });
            this.variable.prev = this.variable.display + operation;
            this.variable.display = 0;
            this.variable.operation = operation
        }
    };

    handleEquals = () => {
        const {display, prev, operation} = this.variable;

        let result;

        switch (operation) {
            case "+":
                result = prev + display;
                break;
            case "-":
                result = prev - display;
                break;
            case "*":
                result = prev * display;
                break;
            case "/":
                result = display !== 0 ? prev / display : 0;
                break;
            default:
                result = display;
        }

        this.setState({
            ...this.variable,
            display: result,
            operation: '',
            prev: ''
        })
    };

    clear = () => {
        this.setState({
            ...this.variable,
            display: 0,
            operation: '',
            prev: '',
        });
        this.variable.display = 0;
        this.variable.prev = '';
        this.variable.operation = '';
        this.sumbit = '';
    };

    handlePercent = () => {
        let {display} = this.variable;


            display = display / 100;

        this.setState({
            ...this.variable,
            display,
            operation: ''
        }, this.handleEquals);
        this.variable.display = display;
        this.variable.operation = '';
    };

    handleChangeValue = () =>{

            this.setState({
                ...this.variable,
                display: 0 - this.variable.display
            });
            this.variable.display = 0 - this.variable.display;
    };

    handleReturn = () =>{
        this.setState({
            ...this.variable,
            display: 0,
            prev: this.variable.prev,
            operation:''
        });
        if(this.variable.operation !== ''){
            this.variable.prev += 0;
        }

            this.sumbit = eval(this.variable.prev+this.variable.display).toString();
        this.variable.display = 0;
    };

    handleFactorial = () => {

        if(this.variable.display <=0){

        }else{
            let sumbit = 1;
            for(let i=1; i<=this.variable.display;i++){
                sumbit *= i;
            }

            this.setState({
                ...this.variable,
                display: sumbit
            });

            this.variable.display = sumbit;
        }
    };

    handleSqrt = () => {

        let val = Math.sqrt(this.variable.display);

        this.setState({
            ...this.variable,
            display: val
        });

        this.variable.display = val;


    };



    handleForPower = (value) =>{


            let sumbit = this.variable.display;
            let val = sumbit;

            for(let i=0; i<value-1; i++){
                sumbit *= val;
            }

            this.setState({
                ...this.variable,
                display: sumbit
            });

            this.variable.display = sumbit;
    };

    handleForPowerTen = () =>{

        let sumbit = 10;

        if(this.variable.display === 0){
                sumbit = 1;
        }else{
            for(let i=0; i<this.variable.display-1; i++){
                sumbit *= 10;
            }
        }

        this.setState({
            ...this.variable,
            display: sumbit
        });

        this.variable.display = sumbit;
    };

    handlePi = () => {

        let sumbit = this.variable.display;

        this.setState({
            ...this.variable,
            display: sumbit
        });

        this.variable.display = sumbit * 3.14159265359;

    };

    e = () => {
        this.setState({
            ...this.variable,
            display: 2.71
        });
        this.variable.display = 2.71;
    };

    handleE = () => {

        let sumbit = 2.71;

        if(this.variable.display === 0){
            sumbit = 1;
        }else{
            for(let i=0; i<this.variable.display-1; i++){
                sumbit *= 2.71;
            }
        }

        this.setState({
            ...this.variable,
            display: sumbit
        });
        this.variable.display = sumbit;
    };

    handleLn = () => {
        let {display} = this.variable;

        if (display < 0) {
            console.log("E: Negative numbers")
        } else if (display === 0) {
            console.log("E: Zero number")
        } else {
            display = Math.log(display)
            this.setState({
                ...this.variable,
                display
            });
            this.variable.display = display;
        }
    }

    handleLog = () => {
        let {display} = this.variable;

        if (display < 0) {
            console.log("E: Negative numbers")
        } else if (display === 0) {
            console.log("E: Zero number")
        } else {
            display = Math.log10(display)
            this.setState({
                ...this.variable,
                display
            })
            this.variable.display = display;
        }
    };


     buttons=[

         {
             color: "FFFFFF",
             text: '_/x',
             onPress: () => this.handleSqrt() ,
             type: Orient.landscape
         },
         {
             color: "FFFFFF",
             text: 'x!',
             onPress: () => this.handleFactorial(),
             type: Orient.landscape
         },
         {
             color: "FFFFFF",
             text: 'AC',
             onPress: () => this.clear(),
             type: Orient.portrait
         },
         {
             color: "FFFFFF",
             text: '+/-',
             onPress: () => this.handleChangeValue(),
             type: Orient.portrait
         },
         {
             color: "FFFFFF",
             text: '%',
             onPress: () => this.handlePercent(),
             type: Orient.portrait
         },
         {
             color: "FFFFFF",
             text: '/',
             onPress: () => this.handleOperation("/"),
             type: Orient.portrait
         },



         {
             color: "FFFFFF",
             text: 'x^2',
             onPress: () => this.handleForPower(2),
             type: Orient.landscape
         },
         {
             color: "FFFFFF",
             text: 'x^3',
             onPress: () => this.handleForPower(3),
             type: Orient.landscape
         },
         {
             color: "FFFFFF",
             text: '1',
             onPress: () => this.handleNumber(1),
             type: Orient.portrait
         },
         {
             color: "FFFFFF",
             text: '2',
             onPress: () => this.handleNumber(2),
             type: Orient.portrait
         },
         {
             color: "FFFFFF",
             text: '3',
             onPress: () => this.handleNumber(3),
             type: Orient.portrait
         },
         {
             color: "FFFFFF",
             text: '*',
             onPress: () => this.handleOperation("*"),
             type: Orient.portrait
         },



         {
             color: "FFFFFF",
             text: 'ln',
             onPress: () => this.handleLn(),
             type: Orient.landscape
         },
         {
             color: "FFFFFF",
             text: 'log10',
             onPress: () => this.handleLog(),
             type: Orient.landscape
         },
         {
             color: "FFFFFF",
             text: '4',
             onPress: () => this.handleNumber(4),
             type: Orient.portrait
         },
         {
             color: "FFFFFF",
             text: '5',
             onPress: () => this.handleNumber(5),
             type: Orient.portrait
         },
         {
             color: "FFFFFF",
             text: '6',
             onPress: () => this.handleNumber(6),
             type: Orient.portrait
         },
         {
             color: "FFFFFF",
             text: '-',
             onPress: () => this.handleOperation("-"),
             type: Orient.portrait
         },

         {
             color: "FFFFFF",
             text: 'e^x',
             onPress: () => this.handleE(),
             type: Orient.landscape
         },
         {
             color: "FFFFFF",
             text: 'e',
            onPress: () => this.e(),
             type: Orient.landscape
         },
         {
             color: "FFFFFF",
             text: '7',
             onPress: () => this.handleNumber(7),
             type: Orient.portrait
         },
         {
             color: "FFFFFF",
             text: '8',
             onPress: () => this.handleNumber(8),
             type: Orient.portrait
         },
         {
             color: "FFFFFF",
             text: '9',
             onPress: () => this.handleNumber(9),
             type: Orient.portrait
         },
         {
             color: "FFFFFF",
             text: '+',
             onPress: () => this.handleOperation("+"),
             type: Orient.portrait
         },

         {
             color: "FFFFFF",
             text: 'π',
             onPress: () => this.handlePi(),
             type: Orient.landscape
         },
         {
             color: "FFFFFF",
             text: '10^x',
             onPress: () => this.handleForPowerTen(),
             type: Orient.landscape
         },
         {
             color: "FFFFFF",
             text: '0',
             onPress: () => this.handleNumber(0),
             type: Orient.portrait
         },
         {
             color: "FFFFFF",
             text: ',',
             onPress: () => this.handleNumber(','),
             type: Orient.portrait
         },
         {
             color: "FFFFFF",
             text: '=',
             onPress: () => this.handleReturn(),
             type: Orient.portrait
         },

    ];

    renderButton = (item) => {
        const {color, text, onPress, type} = item;

        let a = "" +  Dimensions.get('window').width;
        let b = "" +  Dimensions.get('window').height;

        if(type === Orient.portrait && b > a){
            return (
                <Button key={text} width={25} color={color} text={text} fn={onPress} type={type}>{text}</Button>
            )
        }else if(a>b){
            return(
            <Button key={text} width={16.6} color={color} text={text} fn={onPress} type={type}>{text}</Button>
)
        }


    };

    renderView = (isPortrait) => {
            if(this.sumbit === ''){
                return (

                    <View style={styles.container}>
                        <StatusBar style="auto"/>

                        <View style={[styles.result, isPortrait ? stylesPortrait.result : stylesLandscape.result]}>
                            <Text style={styles.resultText}>{this.variable.prev + this.variable.display}</Text>
                        </View>

                        <View style={[styles.keys, isPortrait ? stylesPortrait.keys : stylesLandscape.keys]}>
                            {this.buttons.map((item) => this.renderButton(item))}
                        </View>
                    </View>
                )
            }else{
                return (

                    <View style={styles.container}>
                        <StatusBar style="auto"/>

                        <View style={[styles.result, isPortrait ? stylesPortrait.result : stylesLandscape.result]}>
                            <Text style={styles.resultText}>{this.sumbit}</Text>
                        </View>

                        <View style={[styles.keys, isPortrait ? stylesPortrait.keys : stylesLandscape.keys]}>
                            {this.buttons.map((item) => this.renderButton(item))}
                        </View>
                    </View>
                )
            }

    };


    render() {
        const {isPortrait} = this.variable;

        return (
            this.renderView(isPortrait)
        );
    }




}



